#Mage2 Module Experius Product Frontend Link in Backend

##Features
Using this feature, the administrator can view the product on the front-end website.
The product-links in the product grid are given for each store and will redirect the administrator to the correct store.
The administrator has to navigate in the admin panel to Catalog -> Products.
The Product-links in "Open Product in Frontend column" are not shown when the product is disabled or set invisible and
are given as Website name with its Store id.

A second useful feature is a product-link "Open In Frontend" on the product-page backend. 
The administrator can be redirected here to the frontend product that corresponds to the selected Store View.

##Installation
This module is built by Experius and can be find at Bitbucket repository: 
https://bitbucket.org/experius/mage2-module-experius-product-frontend-link-in-backend/src/master/

For installation no configuration has to be set, only installation and enabling the module is needed.


####Step 1  
In the terminal go to app/code/Experius and run the following command line:
git clone git@bitbucket.org:experius/mage2-module-experius-product-frontend-link-in-backend.git


####Step 2 
Enable extension ("cd" to {Magento root} folder)
  php -f bin/magento module:enable ProductFrontendLinkInBackend
  php -f bin/magento setup:upgrade


##FAQ
Where can I find the Product Links?
Go to your admin
Open Catalog 
Open "Product
Product links for each store can be find under the column 'Open in Frontend'

##Example screenshot of product-in-frontend in the grid table.

![Scheme](Docs/Screenshots/productLink.jpg)


##Example screenshot of product-in-frontend on the Product Page.

![Scheme](Docs/Screenshots/productViewLink.jpg)

